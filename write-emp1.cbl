       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. Son.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 ENDOF-EMP-FILE       VALUE HIGH-VALUE.
           05 EMP-SSN              PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME       PIC X(15).
              10 EMP-FORENAME      PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB           PIC 9(4).
              10 EMP-MOB           PIC 9(2).
              10 EMP-DOB           PIC 9(2).
           05 EMP-GENDER           PIC X.

       PROCEDURE DIVISION.
       Begin.
           OPEN OUTPUT EMP-FILE
           MOVE "621600058" TO EMP-SSN   
           MOVE "SURAK" TO EMP-SURNAME
           MOVE "YANNAPON" TO EMP-FORENAME 
           MOVE "20001112" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "020210101" TO EMP-SSN
           MOVE "TISONTHI" TO EMP-SURNAME
           MOVE "NAPHAT" TO EMP-FORENAME 
           MOVE "20000911" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "85412131" TO EMP-SSN
           MOVE "NOPAKIAT" TO EMP-SURNAME
           MOVE "CHENPOB" TO EMP-FORENAME 
           MOVE "20000820" TO EMP-DATE-OF-BIRTH
           MOVE "F" TO EMP-GENDER
           WRITE EMP-DETAILS

           CLOSE EMP-FILE
           GOBACK 
       .
