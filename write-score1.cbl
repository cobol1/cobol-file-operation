       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. Son.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  SCORE-FILE.
       01  SCORE-DETAILS.
           05 STU-ID         PIC 9(8).
           05 MIDTERM-SCORE  PIC 9(2)V9(2).
           05 FINAL-SCORE    PIC 9(2)V9(2).
           05 PROJECT-SCORE  PIC 9(2)V9(2).

       PROCEDURE DIVISION.
       Begin.
           OPEN OUTPUT SCORE-FILE
           MOVE "62160058" TO STU-ID
           MOVE "35" TO MIDTERM-SCORE 
           MOVE "26" TO FINAL-SCORE 
           MOVE "14" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS 

           MOVE "62160055" TO STU-ID
           MOVE "35" TO MIDTERM-SCORE 
           MOVE "25" TO FINAL-SCORE 
           MOVE "16" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS 

           MOVE "62160052" TO STU-ID
           MOVE "36" TO MIDTERM-SCORE 
           MOVE "40" TO FINAL-SCORE 
           MOVE "5" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS

           MOVE "62160006" TO STU-ID
           MOVE "35" TO MIDTERM-SCORE 
           MOVE "20" TO FINAL-SCORE 
           MOVE "20" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS  

           MOVE "62160249" TO STU-ID
           MOVE "30" TO MIDTERM-SCORE 
           MOVE "30" TO FINAL-SCORE 
           MOVE "20" TO PROJECT-SCORE 
           WRITE SCORE-DETAILS  
           CLOSE SCORE-FILE 
           GOBACK 
           .
           
